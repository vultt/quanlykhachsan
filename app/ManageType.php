<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageType extends Model {

    protected $table = 'manage_types';

    protected $fillable = ['id','name'];

    public $timestamps = false;
}
