<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class KhachsanPhong extends Model {

protected $table = 'khachsan_phong';

 protected $fillable = ['id','name'];

 public $timestamps = true;

}
