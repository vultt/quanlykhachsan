-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 08 Juillet 2017 à 07:12
-- Version du serveur :  5.6.25
-- Version de PHP :  5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_quanlykho`
--

-- --------------------------------------------------------

--
-- Structure de la table `hotel_extras`
--
DROP TABLE IF EXISTS `hotel_extras`;
CREATE TABLE IF NOT EXISTS `hotel_extras` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `cost` int(10) unsigned NOT NULL,
  `hotel_management_id` int(10) unsigned NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `hotel_extras`
--

INSERT INTO `hotel_extras` (`id`, `name`, `num`, `cost`, `hotel_management_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '', 0, 0, 13, 0, '2017-07-08 04:53:59', '2017-07-08 04:53:59'),
(2, 'Nước suối', 1, 20000, 3, 0, '2017-07-08 05:10:41', '2017-07-08 05:10:41'),
(3, 'Coca cola', 1, 40000, 3, 0, '2017-07-08 05:10:41', '2017-07-08 05:10:41');

-- --------------------------------------------------------

--
-- Structure de la table `hotel_managements`
--
DROP TABLE IF EXISTS `hotel_managements`;
CREATE TABLE IF NOT EXISTS `hotel_managements` (
  `id` int(10) unsigned NOT NULL,
  `manage_id` int(10) unsigned NOT NULL,
  `hotel_room_id` int(10) unsigned NOT NULL,
  `hotel_customer_id` int(10) unsigned NOT NULL,
  `time_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_out` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cost` int(10) NOT NULL,
  `rent_type` int(1) NOT NULL DEFAULT '0',
  `rent_num` int(10) NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `hotel_managements`
--

INSERT INTO `hotel_managements` (`id`, `manage_id`, `hotel_room_id`, `hotel_customer_id`, `time_in`, `time_out`, `cost`, `rent_type`, `rent_num`, `note`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 0, '2017-07-19 05:32:30', '2017-07-20 01:31:29', 50000, 1, 3, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 3, 0, '2017-07-20 06:27:13', '2017-07-26 03:00:24', 0, 0, 5, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 3, 0, '2017-07-20 06:27:13', '2017-07-26 03:00:24', 10000, 0, 5, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23400000, 0, 1, '', 0, '2017-07-06 20:16:48', '2017-07-06 20:16:48'),
(5, 1, 2, 8, '2017-07-06 20:25:06', '2017-07-08 05:00:00', 23400000, 0, 1, '', 0, '2017-07-06 20:25:06', '2017-07-06 20:25:06'),
(6, 1, 2, 10, '2017-07-06 20:26:29', '2017-07-07 03:26:29', 320000, 1, 7, '', 0, '2017-07-06 20:26:29', '2017-07-06 20:26:29'),
(7, 1, 2, 11, '2017-07-06 20:27:17', '2017-07-06 21:27:17', 20000, 1, 1, '', 0, '2017-07-06 20:27:17', '2017-07-06 20:27:17'),
(8, 1, 2, 12, '2017-07-07 03:32:21', '2017-07-07 04:32:21', 20000, 1, 1, '', 0, '2017-07-07 03:32:21', '2017-07-07 03:32:21'),
(9, 1, 2, 13, '2017-07-07 03:32:39', '2017-07-08 05:00:00', 23400000, 0, 1, '', 0, '2017-07-07 03:32:39', '2017-07-07 03:32:39'),
(10, 1, 2, 14, '2017-07-07 03:33:53', '2017-07-07 04:33:53', 20000, 1, 1, '', 0, '2017-07-07 03:33:53', '2017-07-07 03:33:53'),
(11, 1, 2, 15, '2017-07-07 03:34:57', '2017-07-08 05:00:00', 23400000, 0, 1, '', 0, '2017-07-07 03:34:57', '2017-07-07 03:34:57'),
(12, 1, 2, 16, '2017-07-07 03:39:23', '2017-07-08 05:00:00', 23400000, 0, 1, '', 0, '2017-07-07 03:39:23', '2017-07-07 03:39:23'),
(13, 1, 2, 17, '2017-07-07 03:40:24', '2017-07-07 18:40:24', 720000, 1, 15, '', 0, '2017-07-07 03:40:24', '2017-07-07 03:40:24');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `hotel_extras`
--
ALTER TABLE `hotel_extras`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `hotel_managements`
--
ALTER TABLE `hotel_managements`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `hotel_extras`
--
ALTER TABLE `hotel_extras`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `hotel_managements`
--
ALTER TABLE `hotel_managements`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
