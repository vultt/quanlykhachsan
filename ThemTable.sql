DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `manage_id` int(10) DEFAULT NULL,
  `manage_type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `role_id`, `manage_id`, `manage_type_id`, `created_at`, `updated_at`) VALUES
(1, 'thanh', 'thanh@a.com', '$2y$10$yJtI5vA.ZFXKWuXZyzgFsOiD7ibwA.cdG0k5phPcLFZpux4XVLczu', 'Q5mKh7bvk8UWCCQTBcIKH413kSLXFZVIrQdAFtEQ', 2, NULL, 1, '2017-06-27 06:27:35', '2017-06-27 06:27:35'),
(3, 'Hoàng Nam - Thủ kho', 'admin@gmail.com', '$2y$10$7LuqenZX8EI2OhVEt4yK.O7q7A.M5F7Co3j6oATlK8zGHGgOqjfLC', 'GyqHEnRODJo3e0xmuPluFW43RC3oksoBvFRkHLWZ9giSC6KKLE8PsTHW66LG', 1, NULL, NULL, '2016-07-03 22:14:22', '2017-06-18 08:12:29'),
(5, 'Trọng Hiếu - Nhân viên', 'han1221@gmail.com', '$2y$10$hRKaeh2Z9fEUnCnrqTWcWOJBHZRtNUQ5rtx3y4I15Ksu5nvj0dg.K', 'EWjpUM02IAb9byjdZ9ju31IiC6ziG1EIz9eQWA1XS8W8jUY9kCuqdvJRq3nu', 1, NULL, NULL, '2016-07-04 03:38:48', '2016-07-04 19:46:18');

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nguoidung_id` (`role_id`);
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

CREATE TABLE IF NOT EXISTS `hotel_prices` (
  `id` int(10) unsigned NOT NULL,
  `price_day` int(10) unsigned NOT NULL,
  `price_hour` int(10) unsigned NOT NULL,
  `price_hour_next` int(10) unsigned NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `nhapkho`
--
ALTER TABLE `hotel_prices`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `hotel_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
CREATE TABLE IF NOT EXISTS `hotel_rooms` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `manage_id` int(10) unsigned NOT NULL,
  `hotel_price_id` int(10) unsigned NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `nhapkho`
--
ALTER TABLE `hotel_rooms`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `hotel_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
CREATE TABLE IF NOT EXISTS `hotel_customers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(30) COLLATE utf8_unicode_ci NULL,
  `hotel_room_id` int(10) unsigned NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `hotel_customers`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `hotel_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
 
CREATE TABLE IF NOT EXISTS `hotel_managements` (
  `id` int(10) unsigned NOT NULL,
  `manage_id` int(10) unsigned NOT NULL,
  `hotel_room_id` int(10) unsigned NOT NULL,
  `time_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_out` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cost` int(10)  NOT NULL,
  `rent_type` int(1) NOT NULL DEFAULT 0,
  `rent_num` int(10)  NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `nhapkho`
--
ALTER TABLE `hotel_managements`
  ADD PRIMARY KEY (`id`);
  ALTER TABLE `hotel_managements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  
CREATE TABLE IF NOT EXISTS `hotel_extras` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `cost` int(10) unsigned NOT NULL,
  `hotel_management_id` int(10) unsigned NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `nhapkho`
--
ALTER TABLE `hotel_extras`
  ADD PRIMARY KEY (`id`);
  ALTER TABLE `hotel_managements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;