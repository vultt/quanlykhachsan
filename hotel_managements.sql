-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2017 at 12:22 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_quanlykho`
--

-- --------------------------------------------------------

--
-- Table structure for table `hotel_managements`
--

DROP TABLE IF EXISTS `hotel_managements`;
CREATE TABLE `hotel_managements` (
  `id` int(10) UNSIGNED NOT NULL,
  `manage_id` int(10) UNSIGNED NOT NULL,
  `hotel_room_id` int(10) UNSIGNED NOT NULL,
  `hotel_customer_id` int(10) UNSIGNED NOT NULL,
  `time_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_out` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `cost` int(10) NOT NULL,
  `rent_type` int(1) NOT NULL DEFAULT '0',
  `rent_num` int(10) NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hotel_managements`
--

INSERT INTO `hotel_managements` (`id`, `manage_id`, `hotel_room_id`, `hotel_customer_id`, `time_in`, `time_expired`, `time_out`, `status`, `cost`, `rent_type`, `rent_num`, `note`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 0, '2017-07-19 05:32:30', '0000-00-00 00:00:00', '2017-07-20 01:31:29', 0, 50000, 1, 3, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 3, 0, '2017-07-20 06:27:13', '0000-00-00 00:00:00', '2017-07-26 03:00:24', 0, 0, 0, 5, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 3, 0, '2017-07-20 06:27:13', '0000-00-00 00:00:00', '2017-07-26 03:00:24', 0, 10000, 0, 5, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-06 20:16:48', '2017-07-06 20:16:48'),
(5, 1, 2, 8, '2017-07-06 20:25:06', '0000-00-00 00:00:00', '2017-07-08 05:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-06 20:25:06', '2017-07-06 20:25:06'),
(6, 1, 2, 10, '2017-07-06 20:26:29', '0000-00-00 00:00:00', '2017-07-07 03:26:29', 0, 320000, 1, 7, '', 0, '2017-07-06 20:26:29', '2017-07-06 20:26:29'),
(7, 1, 2, 11, '2017-07-06 20:27:17', '0000-00-00 00:00:00', '2017-07-06 21:27:17', 0, 20000, 1, 1, '', 0, '2017-07-06 20:27:17', '2017-07-06 20:27:17'),
(8, 1, 2, 12, '2017-07-07 03:32:21', '0000-00-00 00:00:00', '2017-07-07 04:32:21', 0, 20000, 1, 1, '', 0, '2017-07-07 03:32:21', '2017-07-07 03:32:21'),
(9, 1, 2, 13, '2017-07-07 03:32:39', '0000-00-00 00:00:00', '2017-07-08 05:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-07 03:32:39', '2017-07-07 03:32:39'),
(10, 1, 2, 14, '2017-07-07 03:33:53', '0000-00-00 00:00:00', '2017-07-07 04:33:53', 0, 20000, 1, 1, '', 0, '2017-07-07 03:33:53', '2017-07-07 03:33:53'),
(11, 1, 2, 15, '2017-07-07 03:34:57', '0000-00-00 00:00:00', '2017-07-08 05:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-07 03:34:57', '2017-07-07 03:34:57'),
(12, 1, 2, 16, '2017-07-07 03:39:23', '0000-00-00 00:00:00', '2017-07-08 05:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-07 03:39:23', '2017-07-07 03:39:23'),
(13, 1, 2, 17, '2017-07-07 03:40:24', '0000-00-00 00:00:00', '2017-07-07 18:40:24', 0, 720000, 1, 15, '', 0, '2017-07-07 03:40:24', '2017-07-07 03:40:24'),
(14, 1, 2, 18, '2017-07-08 11:00:27', '0000-00-00 00:00:00', '2017-07-08 14:15:00', 0, 220000, 1, 5, '', 0, '2017-07-08 11:00:27', '2017-07-08 11:00:27'),
(15, 1, 3, 20, '2017-07-09 09:18:47', '0000-00-00 00:00:00', '2017-07-10 05:00:00', 0, 23400000, 0, 1, '', 0, '2017-07-09 09:18:47', '2017-07-09 09:18:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hotel_managements`
--
ALTER TABLE `hotel_managements`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hotel_managements`
--
ALTER TABLE `hotel_managements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
